package com.mochen.sakuraproject.api;

public class Api {
    //网站vip解析api
    public final static String SOURCE_1_API = "http://jx.618g.com/?url=";
    public final static String SOURCE_2_API = "http://player.jfrft.net/index.php?url=";
    public final static String SOURCE_3_API = "http://jx.yylep.com/?url=";
    public final static String SOURCE_4_API = "https://sg.hackmg.com/index.php?url=";
    public final static String SOURCE_5_API = "http://jqaaa.com/jx.php?url=";
    public final static String SOURCE_6_API = "http://jx.skyfollowsnow.pro/?url=";
    //检测更新
    public final static String CHECK_UPDATE = "https://gitee.com/mochensignor/SakuraAnime/releases";
    //樱花动漫解析Api
    public final static String PARSE_API = "http://tup.yhdm.tv/?vid=%s";
}
