package com.mochen.sakuraproject.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import org.jsoup.helper.StringUtil;

import java.util.List;

import com.mochen.sakuraproject.R;
import com.mochen.sakuraproject.bean.HomeWekBean;
import com.mochen.sakuraproject.util.Utils;

public class FragmentAdapter extends BaseQuickAdapter<HomeWekBean, BaseViewHolder> {
    private Context context;

    public FragmentAdapter(Context context, List<HomeWekBean> data) {
        super(R.layout.item_home_week, data);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, HomeWekBean item) {
//        helper.addOnClickListener(R.id.drama);
        helper.setText(R.id.title, item.getTitle());
        helper.setText(R.id.drama, item.getDrama());
        if (!StringUtil.isBlank(item.getLabel())) {
            helper.setText(R.id.labelName, Utils.getString(R.string.lastView) + item.getLabel());
        }
        Utils.setDefaultImage(context, item.getImgUrl(), helper.getView(R.id.img));
    }
}
