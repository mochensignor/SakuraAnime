package com.mochen.sakuraproject.main.favorite;

import java.util.List;

import com.mochen.sakuraproject.R;
import com.mochen.sakuraproject.bean.AnimeListBean;
import com.mochen.sakuraproject.database.DatabaseUtil;
import com.mochen.sakuraproject.util.Utils;

public class FavoriteModel implements FavoriteContract.Model {

    @Override
    public void getData(FavoriteContract.LoadDataCallback callback) {
        List<AnimeListBean> list = DatabaseUtil.queryAllFavorite();
        if (list.size() > 0)
            callback.success(list);
        else
            callback.error(Utils.getString(R.string.empty_favorite));
    }
}
