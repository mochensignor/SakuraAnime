package com.mochen.sakuraproject.main.desc;

import com.mochen.sakuraproject.bean.AnimeDescListBean;
import com.mochen.sakuraproject.bean.AnimeListBean;
import com.mochen.sakuraproject.main.base.BaseLoadDataCallback;
import com.mochen.sakuraproject.main.base.BaseView;


public interface DescContract {
    interface Model {
        void getData(String url, LoadDataCallback callback);
    }

    interface View extends BaseView {
        void showSuccessMainView(AnimeDescListBean bean);

        void showSuccessDescView(AnimeListBean bean);

        void showSuccessFavorite(boolean favorite);
    }

    interface LoadDataCallback extends BaseLoadDataCallback {
        void successMain(AnimeDescListBean bean);

        void successDesc(AnimeListBean bean);

        void isFavorite(boolean favorite);
    }
}
