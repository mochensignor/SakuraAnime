package com.mochen.sakuraproject.main.search;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import com.mochen.sakuraproject.R;
import com.mochen.sakuraproject.application.Sakura;
import com.mochen.sakuraproject.bean.AnimeListBean;
import com.mochen.sakuraproject.main.base.BaseModel;
import com.mochen.sakuraproject.net.HttpGet;
import com.mochen.sakuraproject.util.Utils;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class SearchModel extends BaseModel implements SearchContract.Model {
    private List<AnimeListBean> list;
    private static final String firstPage = "page=1";

    @Override
    public void getData(String url, String keyword, int page, boolean isMain, SearchContract.LoadDataCallback callback) {
        if (keyword == null) {
            String hanzi = url.replaceAll("\\s*","").replaceAll("[^(\\u4e00-\\u9fa5)]","");
            try {
                String gbkHanzi = URLEncoder.encode(hanzi, "GBK");
                url = url.replace(hanzi, gbkHanzi);
            } catch (Exception ignored) {
            }
            String pageInfo = "page=" + page;
            if (url.contains(firstPage)) {
                url = url.replace(firstPage, pageInfo);
            } else {
                url = url + "&" + pageInfo;
            }
            url = (url.contains(Sakura.DOMAIN) ? url : Sakura.DOMAIN + url);
            getHtml(url, isMain, callback);
        } else {
            try {
                keyword = URLEncoder.encode(keyword, "GBK");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            url = (url.contains(Sakura.DOMAIN) ? url : Sakura.DOMAIN + url) + "?searchword=" + keyword + "&page=" + page;
            getHtml(url, isMain, callback);
        }
    }

    private void getHtml(String url, boolean isMain, SearchContract.LoadDataCallback callback) {
        callback.log(url);
        new HttpGet(url, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.error(e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) {
                try {
                    Document doc = Jsoup.parse(new String(response.body().bytes(), "GBK"));
                    if (hasRedirected(doc))
                        getHtml(Sakura.DOMAIN + getRedirectedStr(doc), isMain, callback);
                    else if (hasRefresh(doc)) getHtml(url, isMain, callback);
                    else {
                        Elements animeList = doc.select("div#contrainer > div.fire > div.pics > ul > li");
                        if (isMain) {
                            Elements pages = doc.select("div.pages");
                            if (pages.size() > 0) {
                                try {
                                    callback.pageCount(Integer.parseInt(pages.get(0).selectFirst("span").text().split("/")[1].replace("页", "")));
                                } catch (Exception e) {
                                    callback.pageCount(1);
                                }
                            }
                        }
                        if (animeList.size() > 0) {
                            list = new ArrayList<>();
                            for (int i = 0, size = animeList.size(); i < size; i++) {
                                AnimeListBean bean = new AnimeListBean();
                                bean.setTitle(animeList.get(i).select("h2").text());
                                bean.setUrl(animeList.get(i).select("h2 > a").attr("href"));
                                bean.setImg(animeList.get(i).select("img").attr("src"));
                                bean.setDesc(animeList.get(i).select("p").text());
                                list.add(bean);
                            }
                            callback.success(isMain, list);
                        } else callback.error(isMain, Utils.getString(R.string.error_msg));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.error(isMain, e.getMessage());
                }
            }
        });
    }
}
