package com.mochen.sakuraproject.main.setting.user;

import java.util.List;

import com.mochen.sakuraproject.R;
import com.mochen.sakuraproject.bean.ApiBean;
import com.mochen.sakuraproject.database.DatabaseUtil;
import com.mochen.sakuraproject.util.Utils;

public class ApiModel implements ApiContract.Model {

    @Override
    public void getData(ApiContract.LoadDataCallback callback) {
        List<ApiBean> list = DatabaseUtil.queryAllApi();
        if (list.size() > 0)
            callback.success(list);
        else
            callback.error(Utils.getString(R.string.no_api));
    }
}
