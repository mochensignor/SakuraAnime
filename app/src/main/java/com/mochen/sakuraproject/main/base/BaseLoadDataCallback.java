package com.mochen.sakuraproject.main.base;

public interface BaseLoadDataCallback {
    void error(String msg);

    void log(String url);
}