package com.mochen.sakuraproject.main.video;

import org.jsoup.Jsoup;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mochen.sakuraproject.application.Sakura;
import com.mochen.sakuraproject.database.DatabaseUtil;
import com.mochen.sakuraproject.main.base.BaseModel;
import com.mochen.sakuraproject.net.HttpGet;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class VideoModel extends BaseModel implements VideoContract.Model {
    private List<String> videoUrlList = new ArrayList<>();

    @Override
    public void getData(String title, String url, VideoContract.LoadDataCallback callback) {
        getHtml(title, url, callback);
    }

    private void getHtml(String title, String url, VideoContract.LoadDataCallback callback) {
        callback.log(url);
        new HttpGet(url, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.error();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Document doc = Jsoup.parse(new String(response.body().bytes(), "GBK"));
                if (hasRedirected(doc))
                    getHtml(title, Sakura.DOMAIN + getRedirectedStr(doc), callback);
                else if (hasRefresh(doc)) getHtml(title, url, callback);
                else {
                    String fid = DatabaseUtil.getAnimeID(title);
                    String label = doc.select("div.ptitle > h1 > span").text().trim();
                    DatabaseUtil.addIndex(fid, url, label);
                    String videoResourcePath = doc.selectFirst("div.player > script").attr("src");
                    if (StringUtil.isBlank(videoResourcePath)) {
                        callback.empty();
                        return;
                    }
                    String[] urlSplit = response.request().url().toString().split("-");
                    int page = Integer.parseInt(urlSplit[urlSplit.length - 2]);
                    int index = Integer.parseInt(urlSplit[urlSplit.length - 1].split("\\.")[0]);
                    parseVideo(Sakura.DOMAIN + videoResourcePath, callback, page, index);
                }
            }
        });
    }

    private void parseVideo(String jsUrl, VideoContract.LoadDataCallback callback, int page, int index) {
        new HttpGet(jsUrl, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.error();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String jsonStr = response.body().string();
                jsonStr = jsonStr.substring(jsonStr.indexOf("["), jsonStr.lastIndexOf("]") + 1).replaceAll("'", "\"");
                JSONArray jsonObject = JSONObject.parseArray(jsonStr);
                String videoInfo = jsonObject.getJSONArray(page).getJSONArray(1).getString(index);
                String[] infos = videoInfo.split("\\$");
                parseVideoUrl(infos[1], infos[2], callback);
            }
        });
    }

    private static final Pattern parseVideoPattern = Pattern.compile("var.+video.+=.+'(.+)'");
    private static final Pattern urlPattern = Pattern.compile("url=(.+)\\}");
    private static final List<String> needParseHost = Arrays.asList("www.cuan.la", "jx.m3u8.tv");
    private void parseVideoUrl(String vid, String type, VideoContract.LoadDataCallback callback) {
        String parseUrl =  "https://saas.jialingmm.net/code.php?type=" + type + "&vid=" + vid;
        new HttpGet(parseUrl, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.error();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String requestUrl = response.request().url().toString();
                for (String host : needParseHost) {
                    if (requestUrl.contains(host)) {
                        videoUrlList.add(response.request().url().toString());
                        callback.success(videoUrlList);
                        return;
                    }
                }
                String content = response.body().string();
                Matcher matcher = parseVideoPattern.matcher(content);
                if (matcher.find()) {
                    System.out.println(matcher.group(1));
                    videoUrlList.add(matcher.group(1));
                    callback.success(videoUrlList);
                } else {
                    matcher = urlPattern.matcher(content);
                    if (matcher.find()) {
                        System.out.println(matcher.group(1));
                        videoUrlList.add(matcher.group(1));
                        callback.success(videoUrlList);
                    } else {
                        callback.empty();
                    }
                }
            }
        });
    }
}
