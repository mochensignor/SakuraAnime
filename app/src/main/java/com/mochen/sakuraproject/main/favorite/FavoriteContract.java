package com.mochen.sakuraproject.main.favorite;

import java.util.List;

import com.mochen.sakuraproject.bean.AnimeListBean;
import com.mochen.sakuraproject.main.base.BaseLoadDataCallback;
import com.mochen.sakuraproject.main.base.BaseView;

public interface FavoriteContract {
    interface Model {
        void getData(LoadDataCallback callback);
    }

    interface View extends BaseView {
        void showSuccessView(List<AnimeListBean> list);
    }

    interface LoadDataCallback extends BaseLoadDataCallback {
        void success(List<AnimeListBean> list);
    }

}
