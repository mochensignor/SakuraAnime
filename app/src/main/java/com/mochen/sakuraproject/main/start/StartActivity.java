package com.mochen.sakuraproject.main.start;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

import butterknife.BindView;
import com.mochen.sakuraproject.R;
import com.mochen.sakuraproject.api.Api;
import com.mochen.sakuraproject.main.base.BaseActivity;
import com.mochen.sakuraproject.main.base.Presenter;
import com.mochen.sakuraproject.main.home.HomeActivity;
import com.mochen.sakuraproject.net.DownloadUtil;
import com.mochen.sakuraproject.net.HttpGet;
import com.mochen.sakuraproject.util.SharedPreferencesUtils;
import com.mochen.sakuraproject.util.StatusBarUtil;
import com.mochen.sakuraproject.util.Utils;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class StartActivity extends BaseActivity {
    @BindView(R.id.check_update)
    LinearLayout linearLayout;
    private ProgressDialog p;
    private String downUrl;
    private Call downCall;

    @Override
    protected Presenter createPresenter() {
        return null;
    }

    @Override
    protected void loadData() {
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.activity_start;
    }

    @Override
    protected void init() {
        SharedPreferencesUtils.setParam(this, "initX5", "init");
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            linearLayout.setVisibility(View.VISIBLE);
            checkUpdate();
        }, 1000);
    }

    @Override
    protected void initBeforeView() {
        StatusBarUtil.setTranslucentForCoordinatorLayout(this, getResources().getColor(R.color.logo_bg));
    }

    private void checkUpdate() {
        new HttpGet(Api.CHECK_UPDATE, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(() -> {
                    application.showErrorToastMsg(Utils.getString(R.string.ck_network_error_start));
                    openMain();
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    Document doc = Jsoup.parse(response.body().string());
                    Elements elements = doc.select("div.release-tag-item");
                    System.out.println(elements.size());
                    Element maxVersionEle = null;
                    int nowVersionCode = Utils.getVersionCode();
                    for (Element element : elements) {
                        try {
                            int versionCode = Integer.parseInt(element.selectFirst("div.release-header").text().trim());
                            if (versionCode > nowVersionCode) {
                                maxVersionEle = element;
                            }
                        } catch (Exception ignored) {
                        }

                    }
                    if (maxVersionEle == null) {
                        runOnUiThread(() -> openMain());
                    }
                    else {
                        Elements links = maxVersionEle.select("div.releases-download-list > div.item > a");
                        String apkUrl = null;
                        for (Element link : links) {
                            String url = link.selectFirst("a").attr("href");
                            if (url.toLowerCase().endsWith("apk")) {
                                if (url.startsWith("/")) {
                                    apkUrl = Utils.getString(R.string.gitee_host) + url;
                                } else {
                                    apkUrl = Utils.getString(R.string.gitee_host) + "/" + url;
                                }
                                break;
                            }
                        }
                        if (apkUrl == null) {
                            runOnUiThread(() -> runOnUiThread(() -> openMain()));
                        }
                        downUrl = apkUrl;
                        String versionName = maxVersionEle.selectFirst("div.tag-name > a > span").text();
                        String updateContent = maxVersionEle.selectFirst("div.content > div.markdown-body").text();
                        runOnUiThread(() -> Utils.findNewVersion(StartActivity.this,
                                versionName,
                                updateContent,
                                (dialog, which) -> {
                                    /*p = Utils.showProgressDialog(StartActivity.this);
                                    p.setButton(ProgressDialog.BUTTON_NEGATIVE, Utils.getString(R.string.cancel), (dialog1, which1) -> {
                                        if (null != downCall)
                                            downCall.cancel();
                                        dialog1.dismiss();
                                    });
                                    p.show();
                                    downNewVersion(downUrl);*/
                                    dialog.dismiss();
                                    Utils.putTextIntoClip(downUrl);
                                    application.showSuccessToastMsg(Utils.getString(R.string.url_copied));
                                    Utils.openBrowser(StartActivity.this, downUrl);
                                },
                                (dialog, which) -> {
                                    dialog.dismiss();
                                    openMain();
                                })
                        );
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * 下载apk
     *
     * @param url 下载地址
     */
    private void downNewVersion(String url) {
        downCall = DownloadUtil.get().downloadApk(url, new DownloadUtil.OnDownloadListener() {
            @Override
            public void onDownloadSuccess(final String fileName) {
                runOnUiThread(() -> {
                    Utils.cancelProDialog(p);
                    Utils.startInstall(StartActivity.this);
                    StartActivity.this.finish();
                });
            }

            @Override
            public void onDownloading(final int progress) {
                runOnUiThread(() -> p.setProgress(progress));
            }

            @Override
            public void onDownloadFailed() {
                runOnUiThread(() -> {
                    Utils.cancelProDialog(p);
                    application.showErrorToastMsg(Utils.getString(R.string.download_error));
                    openMain();
                });
            }
        });
    }

    private void openMain() {
        startActivity(new Intent(StartActivity.this, HomeActivity.class));
        StartActivity.this.finish();
    }
}
