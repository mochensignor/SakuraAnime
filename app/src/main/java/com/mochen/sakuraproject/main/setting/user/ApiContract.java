package com.mochen.sakuraproject.main.setting.user;

import java.util.List;

import com.mochen.sakuraproject.bean.ApiBean;
import com.mochen.sakuraproject.main.base.BaseLoadDataCallback;
import com.mochen.sakuraproject.main.base.BaseView;

public interface ApiContract {
    interface Model {
        void getData(LoadDataCallback callback);
    }

    interface View extends BaseView {
        void showSuccess(List<ApiBean> list);
    }

    interface LoadDataCallback extends BaseLoadDataCallback {
        void success(List<ApiBean> list);
    }
}
