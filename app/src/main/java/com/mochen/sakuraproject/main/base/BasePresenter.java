package com.mochen.sakuraproject.main.base;

public interface BasePresenter {
    void loadData(boolean isMain);
}
