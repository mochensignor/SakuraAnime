package com.mochen.sakuraproject.main.search;

import java.util.List;

import com.mochen.sakuraproject.bean.AnimeListBean;
import com.mochen.sakuraproject.main.base.BasePresenter;
import com.mochen.sakuraproject.main.base.Presenter;
import com.mochen.sakuraproject.main.tag.TagActivity;

public class SearchPresenter extends Presenter<SearchContract.View> implements BasePresenter, SearchContract.LoadDataCallback {
    private String url;
    private String keyword;
    private int page;
    private SearchContract.View view;
    private SearchModel model;

    public SearchPresenter(String url, String keyword, int page, SearchContract.View view) {
        super(view);
        this.url = url;
        this.keyword = keyword;
        this.page = page;
        this.view = view;
        model = new SearchModel();
    }

    public SearchPresenter(String url, int page, TagActivity view) {
        super(view);
        this.url = url;
        this.page = page;
        this.view = view;
        model = new SearchModel();
    }

    @Override
    public void loadData(boolean isMain) {
        if (isMain) {
            view.showLoadingView();
            view.showEmptyVIew();
        }
        model.getData(url, keyword, page, isMain, this);
    }

    @Override
    public void success(boolean isMain, List<AnimeListBean> list) {
        view.showSuccessView(isMain, list);
    }

    @Override
    public void error(boolean isMain, String msg) {
        view.showErrorView(isMain, msg);
    }

    @Override
    public void pageCount(int pageCount) {
        view.getPageCount(pageCount);
    }

    @Override
    public void error(String msg) {

    }

    @Override
    public void log(String url) {
        view.showLog(url);
    }
}
