package com.mochen.sakuraproject.main.home;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.mochen.sakuraproject.R;
import com.mochen.sakuraproject.adapter.FragmentAdapter;
import com.mochen.sakuraproject.application.Sakura;
import com.mochen.sakuraproject.bean.HomeWekBean;
import com.mochen.sakuraproject.database.DatabaseUtil;
import com.mochen.sakuraproject.main.base.LazyFragment;
import com.mochen.sakuraproject.main.desc.DescActivity;
import com.mochen.sakuraproject.net.HttpGet;
import com.mochen.sakuraproject.util.Utils;
import com.mochen.sakuraproject.util.VideoUtils;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

@SuppressLint("ValidFragment")
public class WeekFragment extends LazyFragment {
    @BindView(R.id.rv_list)
    RecyclerView recyclerView;
    @BindView(R.id.loading)
    ProgressBar loading;
    private FragmentAdapter adapter;
    private List<HomeWekBean> list = new ArrayList<>();
    private Sakura application;
    private View view;
    private View errorView;
    private TextView errorTitle;
    private String week;
    private Unbinder mUnBinder;

    public WeekFragment(String week) {
        this.week = week;
    }

    @Override
    protected View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_week, container, false);
            mUnBinder = ButterKnife.bind(this, view);
        } else {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        errorView = getLayoutInflater().inflate(R.layout.base_error_view, (ViewGroup) recyclerView.getParent(), false);
        errorTitle = errorView.findViewById(R.id.title);
        if (Utils.checkHasNavigationBar(getActivity())) recyclerView.setPadding(0,0,0, Utils.getNavigationBarHeight(getActivity()));
        if (application == null) application = (Sakura) getActivity().getApplication();
        initAdapter();
        return view;
    }

    @Override
    protected void initData() {
        initWeekData();
    }

    public void initAdapter() {
        if (adapter == null) {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
            adapter = new FragmentAdapter(getActivity(), list);
            adapter.openLoadAnimation();
            adapter.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
            adapter.setOnItemClickListener((adapter, view, position) -> {
                if (!Utils.isFastClick()) return;
                HomeWekBean bean = (HomeWekBean) adapter.getItem(position);
                Bundle bundle = new Bundle();
                bundle.putString("name", bean.getTitle());
                bundle.putString("url", VideoUtils.getUrl(bean.getUrl()));
                startActivity(new Intent(getActivity(), DescActivity.class).putExtras(bundle));
            });
//            adapter.setOnItemChildClickListener((adapter, view, position) -> {
//                switch (view.getId()) {
//                    case R.id.drama:
//                        HomeWekBean bean = (HomeWekBean) adapter.getItem(position);
//                        Sakura.getInstance().showToastMsg(bean.getDrama());
//                        break;
//                }
//            });
            recyclerView.setAdapter(adapter);
        }
    }

    private void initWeekData() {
        loading.setVisibility(View.GONE);
        if (adapter.getData().isEmpty()) {
            list = getList(week);
            if (list.size() == 0) {
                errorTitle.setText(application.error);
                adapter.setEmptyView(errorView);
            } else
                adapter.setNewData(list);
        }
    }

    private List getList(String week) {
        list = new ArrayList<>();
        if (application.week.length() > 0) {
            try {
                JSONArray arr = new JSONArray(application.week.getString(week));
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject object = new JSONObject(arr.getString(i));
                    HomeWekBean homeWekBean = new HomeWekBean(object.getString("title"),
                            object.getString("url"),
                            object.getString("drama"),
                            object.getString("dramaUrl"));
                    String fid = DatabaseUtil.getAnimeID(homeWekBean.getTitle());
                    if (!StringUtil.isBlank(fid)) {
                        String label = DatabaseUtil.queryNewestIndex(fid);
                        homeWekBean.setLabel(label);
                    }
                    list.add(homeWekBean);
                }
                generateImgs(list);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    private void generateImgs(List<HomeWekBean> list) {
        if (list == null) {
            return;
        }
        List<String> titles = new ArrayList<>();
        for (HomeWekBean homeWekBean : list) {
            titles.add(homeWekBean.getTitle());
        }
        Map<String, String> animeImgMap = DatabaseUtil.getAnimeImg(titles);
        for (HomeWekBean homeWekBean : list) {
            String img = animeImgMap.get(homeWekBean.getTitle());
            if (StringUtil.isBlank(img)) {
                getAnimeImg(homeWekBean);
            } else {
                homeWekBean.setImgUrl(img);
            }
        }
    }

    private void getAnimeImg(HomeWekBean homeWekBean) {
        new HttpGet(Sakura.DOMAIN + homeWekBean.getUrl(), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    Document body = Jsoup.parse(new String(response.body().bytes(), "GBK"));
                    String imgUrl = body.select("div.tpic > img").attr("src");
                    if (!StringUtil.isBlank(imgUrl)) {
                        homeWekBean.setImgUrl(imgUrl);
                        DatabaseUtil.addAnime(homeWekBean.getTitle(), homeWekBean.getImgUrl());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnBinder.unbind();
    }
}
