package com.mochen.sakuraproject.main.about;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Environment;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.r0adkll.slidr.Slidr;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import com.mochen.sakuraproject.R;
import com.mochen.sakuraproject.adapter.LogAdapter;
import com.mochen.sakuraproject.api.Api;
import com.mochen.sakuraproject.application.Sakura;
import com.mochen.sakuraproject.bean.LogBean;
import com.mochen.sakuraproject.main.base.BaseActivity;
import com.mochen.sakuraproject.main.base.Presenter;
import com.mochen.sakuraproject.net.DownloadUtil;
import com.mochen.sakuraproject.net.HttpGet;
import com.mochen.sakuraproject.util.SharedPreferencesUtils;
import com.mochen.sakuraproject.util.SwipeBackLayoutUtil;
import com.mochen.sakuraproject.util.Utils;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class AboutActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cache)
    TextView cache;
    @BindView(R.id.version)
    TextView version;
    private ProgressDialog p;
    private String downloadUrl;
    private Call downCall;
    @BindView(R.id.footer)
    LinearLayout footer;
    @BindView(R.id.show)
    CoordinatorLayout show;

    @Override
    protected Presenter createPresenter() {
        return null;
    }

    @Override
    protected void loadData() {
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.activity_about;
    }

    @Override
    protected void init() {
        Slidr.attach(this, Utils.defaultInit());
        initToolbar();
        initViews();
    }

    @Override
    protected void initBeforeView() {
        SwipeBackLayoutUtil.convertActivityToTranslucent(this);
    }

    public void initToolbar() {
        toolbar.setTitle(Utils.getString(R.string.about));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());
    }

    private void initViews() {
        LinearLayout.LayoutParams Params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Utils.getNavigationBarHeight(this));
        footer.findViewById(R.id.footer).setLayoutParams(Params);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) show.getLayoutParams();
        params.setMargins(10, 0, 10, Utils.getNavigationBarHeight(this) - 5);
        show.setLayoutParams(params);
        version.setText(Utils.getASVersionName());
        cache.setText(Environment.getExternalStorageDirectory() + Utils.getString(R.string.cache_text));
    }

    @OnClick({R.id.sakura,R.id.github, R.id.check_update})
    public void openBrowser(RelativeLayout relativeLayout) {
        switch (relativeLayout.getId()) {
            case R.id.sakura:
                Utils.viewInChrome(this, Sakura.DOMAIN);
                break;
            case R.id.github:
                Utils.viewInChrome(this, Utils.getString(R.string.github_url));
                break;
            case R.id.check_update:
                if (Utils.isFastClick()) checkUpdate();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.about_menu, menu);
        MenuItem updateLogItem = menu.findItem(R.id.update_log);
        MenuItem openSourceItem = menu.findItem(R.id.open_source);
        if (!(Boolean) SharedPreferencesUtils.getParam(this, "darkTheme", false)) {
            updateLogItem.setIcon(R.drawable.baseline_insert_chart_outlined_black_48dp);
            openSourceItem.setIcon(R.drawable.baseline_all_inclusive_black_48dp);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.update_log:
                showUpdateLogs();
                break;
            case R.id.open_source:
                if (Utils.isFastClick()) startActivity(new Intent(AboutActivity.this,OpenSourceActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showUpdateLogs() {
        AlertDialog alertDialog;
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_update_log, null);
        RecyclerView logs = view.findViewById(R.id.rv_list);
        logs.setLayoutManager(new LinearLayoutManager(this));
        LogAdapter logAdapter = new LogAdapter(createUpdateLogList());
        logs.setAdapter(logAdapter);
        builder.setPositiveButton(Utils.getString(R.string.page_positive), null);
        TextView title = new TextView(this);
        title.setText(Utils.getString(R.string.update_log));
        title.setPadding(30,30,30,30);
        title.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        title.setGravity(Gravity.LEFT);
        title.setTextSize(18);
        title.setTextColor(getResources().getColor(R.color.text_color_primary));
        builder.setCustomTitle(title);
        alertDialog = builder.setView(view).create();
        alertDialog.show();
    }

    public List createUpdateLogList() {
        List logsList = new ArrayList();
        logsList.add(new LogBean("版本：1.2.1","修复部分番剧解析失败bug"));
        logsList.add(new LogBean("版本：1.2.0","1.修复部分番剧解析失败\n2.完善各个菜单功能\n3.修复一些已知的bug"));
        logsList.add(new LogBean("版本：1.1.3","1.修复部分番剧解析失败\n2.修复动漫电影无法展示"));
        logsList.add(new LogBean("版本：1.1.2","优化视频解析速度"));
        logsList.add(new LogBean("版本：1.1.1","修复播放记录展示Bug"));
        logsList.add(new LogBean("版本：1.1.0","修复新番时间表封面展示Bug"));
        logsList.add(new LogBean("版本：1.0.1","添加检测更新"));
        logsList.add(new LogBean("版本：1.0.0","第一个版本（可能存在许多Bug~）"));
        return logsList;
    }

    public void checkUpdate() {
        p = Utils.getProDialog(this, R.string.check_update_text);
        new Handler().postDelayed(() -> new HttpGet(Api.CHECK_UPDATE, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(() -> {
                    Utils.cancelProDialog(p);
                    application.showSnackbarMsgAction(show, Utils.getString(R.string.ck_network_error_start), Utils.getString(R.string.try_again), v -> checkUpdate());
                });
            }

            @Override
            public void onResponse(Call call, Response response) {
                try {
                    Document doc = Jsoup.parse(response.body().string());
                    Elements elements = doc.select("div.release-tag-item");
                    System.out.println(elements.size());
                    Element maxVersionEle = null;
                    int nowVersionCode = Utils.getVersionCode();
                    for (Element element : elements) {
                        try {
                            int versionCode = Integer.parseInt(element.selectFirst("div.release-header").text().trim());
                            if (versionCode > nowVersionCode) {
                                maxVersionEle = element;
                            }
                        } catch (Exception ignored) {
                        }

                    }
                    if (maxVersionEle == null) {
                        runOnUiThread(() -> {
                            Utils.cancelProDialog(p);
                            application.showSnackbarMsg(show, Utils.getString(R.string.no_new_version));
                        });
                    } else {
                        Elements links = maxVersionEle.select("div.releases-download-list > div.item > a");
                        String apkUrl = null;
                        for (Element link : links) {
                            String url = link.selectFirst("a").attr("href");
                            if (url.toLowerCase().endsWith("apk")) {
                                if (url.startsWith("/")) {
                                    apkUrl = Utils.getString(R.string.gitee_host) + url;
                                } else {
                                    apkUrl = Utils.getString(R.string.gitee_host) + "/" + url;
                                }
                                break;
                            }
                        }
                        if (apkUrl == null) {
                            runOnUiThread(() -> {
                                Utils.cancelProDialog(p);
                                application.showSnackbarMsg(show, Utils.getString(R.string.no_new_version));
                            });
                        }
                        downloadUrl = apkUrl;
                        String versionName = maxVersionEle.selectFirst("div.tag-name > a > span").text();
                        String updateContent = maxVersionEle.selectFirst("div.content > div.markdown-body").text();
                        runOnUiThread(() -> {
                            Utils.cancelProDialog(p);
                            Utils.findNewVersion(AboutActivity.this,
                                    versionName,
                                    updateContent,
                                    (dialog, which) -> {
//                                        download();
                                        dialog.dismiss();
                                        Utils.putTextIntoClip(downloadUrl);
                                        application.showSuccessToastMsg(Utils.getString(R.string.url_copied));
                                        Utils.openBrowser(AboutActivity.this, downloadUrl);
                                    },
                                    (dialog, which) -> dialog.dismiss()
                            );
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }), 1000);
    }

    public void download() {
        p = Utils.showProgressDialog(AboutActivity.this);
        p.setButton(ProgressDialog.BUTTON_NEGATIVE, Utils.getString(R.string.page_negative), (dialog1, which1) -> {
            if (null != downCall)
                downCall.cancel();
            dialog1.dismiss();
        });
        p.show();
        downNewVersion(downloadUrl);
    }

    /**
     * 下载apk
     *
     * @param url 下载地址
     */
    private void downNewVersion(String url) {
        downCall = DownloadUtil.get().downloadApk(url, new DownloadUtil.OnDownloadListener() {
            @Override
            public void onDownloadSuccess(final String fileName) {
                runOnUiThread(() -> {
                    Utils.cancelProDialog(p);
                    Utils.startInstall(AboutActivity.this);
                });
            }

            @Override
            public void onDownloading(final int progress) {
                runOnUiThread(() -> p.setProgress(progress));
            }

            @Override
            public void onDownloadFailed() {
                runOnUiThread(() -> {
                    Utils.cancelProDialog(p);
                    application.showSnackbarMsgAction(show, Utils.getString(R.string.download_error), Utils.getString(R.string.try_again), v -> download());
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10001) {
            Utils.startInstall(AboutActivity.this);
        }
    }
}
