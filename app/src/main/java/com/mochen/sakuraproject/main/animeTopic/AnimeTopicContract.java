package com.mochen.sakuraproject.main.animeTopic;

import java.util.List;

import com.mochen.sakuraproject.bean.AnimeListBean;
import com.mochen.sakuraproject.main.base.BaseLoadDataCallback;
import com.mochen.sakuraproject.main.base.BaseView;

public interface AnimeTopicContract {
    interface Model {
        void getData(String url, int page, boolean isMain, LoadDataCallback callback);
    }

    interface View extends BaseView {
        void showSuccessView(boolean isMain, List<AnimeListBean> list);

        void showErrorView(boolean isMain, String msg);

        void getPageCountSuccessView(int count);
    }

    interface LoadDataCallback extends BaseLoadDataCallback {
        void success(boolean isMain, List<AnimeListBean> list);

        void error(boolean isMain, String msg);

        void pageCount(int count);
    }
}
