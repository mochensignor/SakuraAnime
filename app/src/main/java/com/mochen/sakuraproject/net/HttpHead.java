package com.mochen.sakuraproject.net;

import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class HttpHead {
    private final static int connectTimeout = 10;
    private final static int readTimeout = 20;

    public HttpHead(String url, Callback callback) {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(connectTimeout, TimeUnit.SECONDS)
                .readTimeout(readTimeout, TimeUnit.SECONDS)
                .build();
        Request request = new Request.Builder()
                .url(url)
                .head()
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }
}
