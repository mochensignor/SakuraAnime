# 樱花动漫  
![](https://img.shields.io/badge/Android-5.0%20or%20above-brightgreen.svg)
使用[jsoup](https://github.com/jhy/jsoup)爬取[樱花动漫](http://www.imomoe.ai/)部分内容编写的第三方客户端。

#### 应用截图
<img src="https://gitee.com/mochensignor/SakuraAnime/blob/master/Screenshots/Screenshot_20201230-110506_%E5%89%AF%E6%9C%AC.jpg?raw=true" />
